import React from 'react';
// J'importe createRoot de react-dom/client
import ReactDOM from 'react-dom';

import App from './Components/App';
import "../src/styles/index.css"


    ReactDOM.render(
      <App/>,
      document.getElementById('root')
    )


/**
 *  const root = ReactDOM.createRoot(document.getElementById('root')); 
    root.render(<App />);
*/