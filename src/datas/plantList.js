import monstera from '../Images/monstera.jpg';


export const plantList = [
	{
		name: 'monstera',
		category: 'classique',
		id: '1ed',
        isBestSale: true,
		water: 2,
		light: 3,
		cover: monstera
	},
	{
		name: 'ficus lyrata',
		category: 'classique',
		id: '2ab',
		water: 3,
		light: 2,
		cover: monstera
	},
	{
		name: 'pothos argenté',
		category: 'classique',
		id: '3sd',
		water: 1,
		light: 2,
		cover: monstera
	},
	{
		name: 'yucca',
		category: 'classique',
		id: '4kk',
		isBestSale: true,
		water: 2,
		light: 2,
		cover: monstera
	},
	{
		name: 'olivier',
		category: 'extérieur',
		id: '5pl',
		water: 1,
		light: 2,
		cover: monstera
	},
	{
		name: 'géranium',
		category: 'extérieur',
		id: '6uo',
        isBestSale: true,
		water: 2,
		light: 3,
		cover: monstera
	},
	{
		name: 'basilique',
		category: 'extérieur',
		id: '7ie',
		water: 3,
		light: 3,
		cover: monstera
	},
	{
		name: 'aloe',
		category: 'plante grasse',
		id: '8fp',
		water: 3,
		light: 3,
		cover: monstera
	},
	{
		name: 'succulente',
		category: 'plante grasse',
		id: '9vn',
		water: 2,
		light: 3,
		cover: monstera
	},
	{
		name: 'succulente',
		category: 'plante grasse',
		id: '9vn',
		water: 2,
		light: 3,
		cover: monstera
	},
	{
		name: 'monstera',
		category: 'classique',
		id: '1ed',
        isBestSale: true,
		water: 2,
		light: 3,
		cover: monstera
	},
	{
		name: 'ficus lyrata',
		category: 'classique',
		id: '2ab',
		water: 3,
		light: 2,
		cover: monstera
	},
	{
		name: 'pothos argenté',
		category: 'classique',
		id: '3sd',
		water: 1,
		light: 2,
		cover: monstera
	},
	{
		name: 'yucca',
		category: 'classique',
		id: '4kk',
		isBestSale: true,
		water: 2,
		light: 2,
		cover: monstera
	},
	{
		name: 'olivier',
		category: 'extérieur',
		id: '5pl',
		water: 1,
		light: 2,
		cover: monstera
	},
	{
		name: 'géranium',
		category: 'extérieur',
		id: '6uo',
        isBestSale: true,
		water: 2,
		light: 3,
		cover: monstera
	},
	{
		name: 'basilique',
		category: 'extérieur',
		id: '7ie',
		water: 3,
		light: 3,
		cover: monstera
	},
	{
		name: 'aloe',
		category: 'plante grasse',
		id: '8fp',
		water: 3,
		light: 3,
		cover: monstera
	},
	{
		name: 'succulente',
		category: 'plante grasse',
		id: '9vn',
		water: 2,
		light: 3,
		cover: monstera
	},
	{
		name: 'succulente',
		category: 'plante grasse',
		id: '9vn',
		water: 2,
		light: 3,
		cover: monstera
	}
]

