// J'importe mes Images
import sun from '../Images/sun.svg';
import water from '../Images/water.svg';


// Ma fonction CareScale
const CareScale = ({scaleValue,careType}) => {

    /**
     * la déstructuration: 
     * Elle permet directement de déclarer une variable et de lui assigner la valeur d'une propriété d'un objet.
     * 
     * const {scaleValue, careType} = props
       // On évite de multiplier les déclarations qui sans cette syntaxe auraient été :
       // const scaleValue = props.scaleValue et
       // const careType = props.careType
     * 
     * En pratique, une prop peut avoir n’importe quelle valeur possible en JavaScript, 
     * mais syntaxiquement, en JSX, on n’a en gros que deux possibilités :
       un littéral  String , matérialisé par des guillemets "";
       ou, pour tout le reste (booléen, number, expression Javascript, etc.), des accolades  {}.
     * 
     * 
     * 
     * 
     * 
    */


    // Mes variables
    const range= [1,2,3];
    const scaleType= 
    // Condition si careType vaut light met l'image sun sinon met l'image water
    careType === 'light'? (<img src={sun} alt="sun-img"/>) : (<img src={water} alt="water-img"/>)
    return (
        <div>
            {
                range.map((RangeElem) => 
                    // condition
                    scaleValue >= RangeElem? (<span key={RangeElem.toString()}>{scaleType}</span>) : null
                )
                
            }
        </div>
    );
};

export default CareScale;


// Faire descendre les données, des parents vers les enfants

/**
 * Les props nous permettent donc de configurer nos composants. 
 * Elles répondent à la logique même de React selon laquelle les données descendent à travers notre arborescence
 * de composants : il s'agit d'un flux de données unidirectionnel.
 * 
 * Un composant est le parent du composant défini dans le  return().
 * 
 * Vous devez garder deux règles à l'esprit :
 * Une prop est toujours passée par un composant parent à son enfant : 
 * c’est le seul moyen normal de transmission.
 * Une prop est considérée en lecture seule dans le composant qui la reçoit.
 * 
 * La prop technique  children:
 * 
 * 
 * 
 * En résumé:
 * Les props sont des objets que l'on peut récupérer dans les paramètres de notre composant fonction.
   Il existe deux syntaxes pour assigner une valeur à une prop :
   les guillemets pour les  string ;
   les accolades pour tout le reste : nombres, expressions JavaScript, booléen, etc.

   La déstructuration est une syntaxe permettant de déclarer une variable en l'affectant directement à la valeur d'un objet (ou tableau).
   Une prop est :
   toujours passée par un composant parent à son enfant ;
   considérée en lecture seule dans le composant qui la reçoit.
   
   La prop  children   est renseignée en imbriquant les enfants dans le parent : <Parent><Enfant /></Parent>.
   children   est utile lorsqu'un composant ne connaît pas ses enfants à l'avance.
 * 
 * 
*/