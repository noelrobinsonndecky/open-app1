import Banner from './banner'
import logo from '../Images/logo.png'
// import Cart from './Cart'
import ShoppingList from './shoppingList'

function App() {
	return (
		<div>
			<Banner>
				<div className='monLogo'>
                    <img src={logo} alt='La maison jungle' className='logo' />
                </div>
				<h1 className='img-titre'>La maison jungle</h1>
			</Banner>
			
			<ShoppingList />
		</div>
	)
}

export default App;