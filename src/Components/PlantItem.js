import CareScale from './CareScale'
import '../styles/PlantItem.css'

function PlantItem({ id, cover, name, water, light }) {
	return (
		<div className='conteneur'>
		    <li key={id} className='plant-item'>
			    <img className='plant-item-cover' src={cover} alt={`${name} cover`} />
			    {name}
			    <div>
				    <CareScale careType='water' scaleValue={water} />
				    <CareScale careType='light' scaleValue={light} />
			    </div>
		    </li>
		</div>
	)
}

export default PlantItem
