import '../styles/banner.css'

function Banner({ children }) {
	return <div className='bannière'>{children}</div>
}

export default Banner