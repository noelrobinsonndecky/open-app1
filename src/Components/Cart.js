const Cart = () => {
    const monstera = 8;
    const lierre = 10;
    const fleurs = 15;
    return (
       <div>
            <h2>Votre Panier</h2>
            <ul>
                <li>Monstera: {monstera} £</li>
                <li>Lierre: {lierre} £</li>
                <li>Fleurs: {fleurs} £</li>
            </ul>
            <br/>
            <h3>Total: {monstera + lierre + fleurs} £</h3>
       </div>
    )
}

export default Cart;