// Styler Notre App react:

/**
 * les classNames:
 * Comme en HTML, nous pouvons associer des attributs à nos éléments.
 * En revanche, il existe des mots réservés en JavaScript, tels que  class.
 * 
 * Il suffit pour cela d'utiliser l'attribut  className  , et de lui préciser une string. 
 * D'ailleurs, vous pouvez utiliser plusieurs classes sur un élément en les mettant à la suite, 
 * séparées par un espace.
 * 
 * En résumé:
 * L'attribut className permet de préciser une classe à un élément React pour lui indiquer du CSS.
   Le fichier CSS correspondant peut être importé directement dans un fichier  .js.
   L'attribut style permet d'intégrer du style directement, on appelle cela du inline style.
   Les images sont importées par React grâce à Webpack. Il suffit d'importer l'image souhaitée.
 * 
 * 
*/


// L'Itération de contenu

/**
 * La méthode map():
 * passe sur chaque élément d'un tableau. Elle lui applique une fonction, 
 * et renvoie un nouveau tableau contenant les résultats de cette fonction appliquée sur chaque élément.
 * Elle va nous permettre de prendre une liste de données, et de la transformer en liste de composants.
 * 
 * La méthode  map()  permet facilement d'itérer sur des données et de retourner un tableau d'éléments. 
 * Comme elle, les méthodes  forEach(),  filter(),  reduce(), etc., qui permettent de manipuler des tableaux, 
 * seront également nos alliés en React.
 * 
 * Qu'est-ce que la prop  key?
 * les  key   (clés) aident React à identifier quels éléments d’une liste ont changé, ont été ajoutés ou supprimés. 
 * Vous devez donner une clé à chaque élément dans un tableau, afin d’apporter aux éléments une identité stable.
 * une  key  doit impérativement respecter deux principes :
   Elle doit être unique au sein du tableau. 
   Et stable dans le temps (pour la même donnée source, on aura toujours la même valeur de key=).
   la  key   permet d'associer une donnée au composant correspondant dans le DOM virtuel qui 
   permettra ensuite de générer les composants. 
 * 
 * 
 * En Résumé:
 * 
 * À partir d’une liste de données,  map()   permet de créer une liste de composants React.
   La prop  key   est indispensable dans les listes de composants.
   Si vous voulez éviter les bugs, la prop  key   doit : 
   être unique au sein de la liste ;
   perdurer dans le temps.
   La best practice pour créer une  key   est d’utiliser l’ id unique associée à une donnée, 
   et de ne pas vous contenter d'utiliser l'index de l'élément dans la liste.
   Une condition ternaire permet d’afficher un élément ou un autre dans le JSX, 
   répondant à la condition "if… else...".
   Il existe d'autres manières de créer des conditions en React, notamment en sortant les conditions du JSX.
 * 
 * 
 * Les props en React:
 * 
 * 
 * 
 * 
*/